## How to run the server?

1. `git clone` this repo and cd` into it.
2. Create a new virtualenv and activate it: `python3 -m venv env && . env/bin/activate`
3. Install all dependencies: `pip install -r requirements.txt`
4. Run migrations: `python manage.py migrate --settings=main.settings.local`
5. Start server: `python manage.py runserver --settings=main.settings.local`
6. The celery processes can be started with something like:
   `DJANGO_SETTINGS_MODULE=applist.settings.local  celery -A applist worker` and

   `DJANGO_SETTINGS_MODULE=applist.settings.local  celery -A applist beat --scheduler django_celery_beat.schedulers:DatabaseScheduler`
