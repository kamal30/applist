from dotenv import find_dotenv, load_dotenv

from celery import Celery

app = Celery('applist')

load_dotenv(find_dotenv())

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')
