from .base import *

SECRET_KEY = '8ihvsv@d%0r7zok1oimlkcol83vu%v@*2(6ldsrt76gw1#lprh'

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
