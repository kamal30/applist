from django.urls import path

from . import views

urlpatterns = [
    path('search/<term>/', views.KeywordRetrieve.as_view()),
    path('fetch/', views.fetch_apps),
    path('reviews/', views.ReviewListCreate.as_view()),
]
