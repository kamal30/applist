from rest_framework import serializers

from .models import App, Keyword, Match, Review


class AppSerializer(serializers.ModelSerializer):
    class Meta:
        model = App
        fields = ('name', 'url')


class MatchSerializer(serializers.ModelSerializer):
    app = AppSerializer(read_only=True)

    class Meta:
        model = Match
        fields = ('rank', 'date', 'app')


class KeywordSerializer(serializers.ModelSerializer):
    match_set = MatchSerializer(read_only=True, many=True)

    class Meta:
        model = Keyword
        fields = ('term', 'match_set')


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = '__all__'
