from rest_framework import generics, permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .models import Keyword, Review
from .serializers import KeywordSerializer, ReviewSerializer
from .tasks import create_new_keyword


class KeywordRetrieve(generics.RetrieveAPIView):
    queryset = Keyword.objects.all()
    serializer_class = KeywordSerializer
    lookup_field = 'term'
    permission_classes = [permissions.IsAuthenticated]


@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def fetch_apps(request):
    term = request.data['term']
    if Keyword.objects.filter(term=term).exists():
        return Response(status=200)
    else:
        create_new_keyword.delay(term)
        return Response(status=201)


class ReviewListCreate(generics.ListCreateAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = [permissions.IsAuthenticated]
