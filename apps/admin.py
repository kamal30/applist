from django.contrib import admin

from .models import App, Keyword, Match, Review


class MatchAdmin(admin.ModelAdmin):
    readonly_fields = ('date',)


admin.site.register(App)
admin.site.register(Keyword)
admin.site.register(Match, MatchAdmin)
admin.site.register(Review)
