import datetime

import gspread
import more_itertools as mit
from django.core.management.base import BaseCommand

from apps.models import Review


class Command(BaseCommand):
    help = 'Load reviews from the given worksheet'

    def add_arguments(self, parser):
        parser.add_argument('spreadsheet')
        parser.add_argument('worksheet')

    def handle(self, *args, **kwargs):
        gc = gspread.service_account()
        sh = gc.open(kwargs['spreadsheet'])
        wks = sh.worksheet(kwargs['worksheet'])
        rows = wks.get_all_values()
        self.stdout.write(f'{len(rows)} rows downloaded.')

        self.stdout.write('updating db.')
        for row in enumerate(rows):
            try:
                row_date = datetime.datetime.strptime(row[6], '%B %d, %Y')
                date = f'{row_date.year}-{row_date.month}-{row_date.day}'
                Review.objects.create(url=row[0], name=row[1], rating=row[2],
                                      text=row[3], review_url=row[4],
                                      image_url=row[5],
                                      date=date)
            except Exception as e:
                self.stderr.write(repr(e))
        self.stdout.write('done.')

        self.stdout.write('deleting {len(rows)}.')
        wks.delete_rows(1, len(rows)-1)
        self.stdout.write('done.')
