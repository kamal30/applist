from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class App(models.Model):
    name = models.CharField(max_length=200)
    url = models.URLField()

    def __str__(self):
        return self.name


class Keyword(models.Model):
    term = models.CharField(max_length=200, db_index=True)
    apps = models.ManyToManyField(
        App, related_name='keywords', through='Match'
    )

    def __str__(self):
        return self.term


class Match(models.Model):
    key = models.ForeignKey(Keyword, on_delete=models.CASCADE)
    app = models.ForeignKey(App, on_delete=models.CASCADE)
    rank = models.PositiveIntegerField()
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.key} -> {self.app} [{self.date.ctime()}]'


class Review(models.Model):
    url = models.URLField()
    name = models.CharField(max_length=200)
    rating = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(5)]
    )
    text = models.TextField()
    review_url = models.URLField(unique=True)
    image_url = models.URLField()
    date = models.DateField()

    def __str__(self):
        return f'{self.name} ({self.rating})'
