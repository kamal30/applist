import sys
import time

from celery import shared_task
from celery.utils.log import get_task_logger
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from .models import App, Keyword, Match

IMPLICIT_WAIT_DURATION = 10

SCROLL_PAUSE_TIME = 1

logger = get_task_logger(__name__)


def get_rankings(term):
    try:
        options = Options()
        options.headless = True
        driver = webdriver.Chrome(options=options)
        driver.implicitly_wait(IMPLICIT_WAIT_DURATION)

        url = f"https://play.google.com/store/search?q={term}&c=apps&hl=en"
        driver.get(url)

        tries = 10  # Try scrolling a few times
        for _ in range(tries):
            elems = driver.find_elements_by_css_selector('.WsMG1c.nnK0zc')
            if len(elems) >= 100:
                break
            driver.find_element_by_tag_name('body').send_keys(Keys.END)
            time.sleep(SCROLL_PAUSE_TIME)

        rankings = []
        for elem in elems:
            name = elem.text
            url = elem.find_element_by_xpath('..').get_attribute('href')
            rankings.append((name, url))
        return rankings

    except Exception:
        return None
    finally:
        driver.quit()


def create_matches(keyword, rankings):
    for i, (name, url) in enumerate(rankings):
        logger.warn(f'Creating new match: ({i}, {name}, {url})')
        app, _ = App.objects.get_or_create(name=name, url=url)
        Match.objects.create(key=keyword, app=app, rank=i+1)


@shared_task
def create_new_keyword(term):
    rankings = get_rankings(term)
    if rankings is None:
        logger.error('Scraping failed. Aborting task.')
        return
    keyword = Keyword.objects.create(term=term)
    create_matches(keyword, rankings)


@shared_task
def update_all_keywords():
    for keyword in Keyword.objects.all():
        rankings = get_rankings(keyword.term)
        if rankings is None:
            logger.error('Scraping failed. Skipping term:', keyword.term)
            continue
        create_matches(keyword, rankings)
